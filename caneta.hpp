#ifndef CANETA_HPP
#define CANETA_HPP

#include <string>
#include <iostream>

class Caneta{

private:
	//Atributos

	std::string cor;	
	std::string marca;
	float preco;

public:
	//Métodos

	Caneta(); //Construtor
	~Caneta(); //Destrutor

	//Métodos de acessos Get/Set
	
	void setCor(std::string cor); 
	std::string getCor();
	
	void setMarca(std::string marca);
	std::string getMarca();
	
	void setPreco(float preco);
	float getPreco();

	//Outro método

	void imprimeDados();
};
#endif

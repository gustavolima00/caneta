#include "caneta.hpp"
#include <iostream>
#include <string>

Caneta::Caneta(){

	std::cout <<"Uma caneta foi construída"<< std::endl;
	cor = "SEM COR";
	marca = "SEM MARCA";
	preco = 0;
}

Caneta::~Caneta(){
	std::cout <<"Uma caneta foi destruída"<< std::endl;
}

void Caneta::setCor(std::string cor){
	this->cor = cor;
}

std::string Caneta::getCor(){
	return cor; 
}

void Caneta::setMarca(std::string marca){
	this->marca=marca;
}

std::string Caneta::getMarca(){
	return marca;
}

void Caneta::setPreco(float preco){
	this->preco=preco;
}

float Caneta::getPreco(){
	return preco;
}

void Caneta::imprimeDados(){
	std::cout << "Cor: " << cor << std::endl;
	std::cout << "Marca: " << marca << std::endl;
	std::cout << "Preço: " << preco << std::endl;
}

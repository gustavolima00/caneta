#include "aluno.hpp"
#include <iostream>

using namespace std;

Aluno::Aluno(){
	cout << "Construtor da classe Aluno" << endl;
	nome = "";
	sexo = "";
	RG = 0;
	CPF = 0;
	email = "";
	matricula = 0;
	creditos = 0;
	ira = 5;
	semestre = 1;
	curso = "";
}
Aluno::~Aluno(){
	cout << "Destrutor da classe aluno" << endl;
}
void Aluno::setCreditos(int creditos){
	this -> creditos = creditos;
}
int Aluno::getCreditos(){
	return creditos;
}
void Aluno::setIra(float ira){
	this -> ira = ira;
}
float Aluno::getIra(){
	return ira;
}
void Aluno::setSemestre(int semestre){
	this -> semestre = semestre;
}
int Aluno::getSemestre(){
	return semestre;
}
void Aluno::setCurso(string curso){
	this -> curso=curso;
}
string Aluno::getCurso(){
	return curso;
}
void Aluno::imprimeDados(){
	cout << "Nome: " << nome << endl;
	cout << "Sexo: " << sexo << endl;
	cout << "RG: " << RG << endl;
	cout << "CPF: " << CPF << endl;
	cout << "Email: " << email << endl;
	cout << "Matricula: " << matricula << endl;
	cout << "Créditos: " << creditos << endl;
	cout << "IRA: " << ira << endl;
	cout << "Semestre: " << semestre << endl;
	cout << "Curso: " << curso << endl;
}

#include "caneta.hpp"
#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"

using namespace std;

int main(int argc, char **argv){

	//Caneta caneta1;
	//Caneta caneta2;

	Pessoa pessoa1;
	Pessoa * pessoa2 = new Pessoa();
	Aluno * aluno1 = new Aluno();
	/*
	Caneta * caneta3;
	caneta3 = new Caneta();

	Caneta * caneta4 = new Caneta();

	caneta3->setCor("Azul");
	caneta3->setMarca("Faber");
	caneta3->setPreco(0.75);

	caneta4->imprimeDados();
	caneta3->imprimeDados();

	delete caneta4;
	delete caneta3;

	caneta1.setCor("Preta");
	caneta1.setMarca("Bic");
	caneta1.setPreco(15.0);

	cout << "Caneta1: " << endl;
	caneta1.imprimeDados();
	cout << "Caneta2: " << endl;
	caneta2.imprimeDados();
	*/
	pessoa2 -> setNome("Matheus Marques");
	pessoa2 -> setSexo("Masculino");
	delete pessoa2;
	aluno1 -> setNome("Gustavo Marques Lima");
	aluno1 -> setMatricula(170035158);
	aluno1 -> setCurso("Engenharia de Software");
	aluno1 -> setIra(4.6833);
	aluno1 -> setSexo("Masculino");
	aluno1 -> setSemestre(3);
	aluno1 -> setCreditos(50);
	aluno1 -> imprimeDados();
	delete aluno1;

	return 0;
}
